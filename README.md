# Sleeping Owl Admin Elements #

This package will provide various additional admin elements for sleepingOwl.

Right now there is just one additional element: *OptionalDateTime*

## Installation ##

Import the package into your Laravel project using composer:

```
composer require worthers/sleeping-owl-admin-elements
```

Add the packages folder to the "psr-4" section in the "autoload" section in composer.json (You can use any namespace you want of course.
):

```
"Worthers\\SleepingOwlAdminElements\\": "vendor/worthers/sleeping-owl-admin-elements/src/"
```


Add provider to config/app.php
```
    /*
     * Package Service Providers...
     */
    \Worthers\SleepingOwlAdminElements\SleepingOwlAdminElementsServiceProvider::class,
```

Publish the assets
```
php artisan vendor:publish  --provider="Worthers\SleepingOwlAdminElements\SleepingOwlAdminElementsServiceProvider"
```


## The elements ##

### Optional Datetime ###

![Optional datetime screenshot](optional-datetime.png)

The Optional Datetime field extends the existing sleepingOwl datetime field but also shows a 'active' checkbox allowing the user to turn the datetime field on/off.

This is useful when a datetime is an optional field and you want the user to be able to say "I don't want to set a datetime".

Usage:

```
AdminFormElement::optionalDatetime('publish_until', 'Publish until')
```

**Ensure your datetime field allows NULL.**

### Image Upload ###

![Image upload screenshot](image-upload.png)

The Image Upload field extends the existing sleepingOwl Upload field but also displays a thumbnail sized copy of the uploaded file.

The sleepingOwl upload field doesn't show what you've uploaded already so this improves on that.

Usage:
```
AdminFormElement::imageUpload('image', 'My Image')
```

