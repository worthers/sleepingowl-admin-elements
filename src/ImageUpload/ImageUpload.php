<?php
namespace Worthers\SleepingOwlAdminElements\ImageUpload;

use SleepingOwl\Admin\Form\Element\Upload;

/**
 * The ImageUpload admin element is the same as the normal Upload element, but it also shows the image that has already been uploaded
 *
 * @package Worthers\SleepingOwlAdminElements
 * @author  Ben Hull <ben@worthers.com>
 */
class ImageUpload extends Upload
{

	/**
	 * Render the HTML view for the admin element
	 *
	 * @author Ben Hull <ben@worthers.com>
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function render()
	{
		view()->addNamespace('worthers.sleeping-owl-admin-elements', __DIR__);

		return view(
			'worthers.sleeping-owl-admin-elements::imageupload',
			$this->toArray()
		);
	}

}
