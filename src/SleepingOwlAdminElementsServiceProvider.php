<?php namespace	Worthers\SleepingOwlAdminElements;

use Illuminate\Support\ServiceProvider;
use KodiCMS\Assets\Facades\Meta;
use KodiCMS\Assets\Facades\PackageManager;
use SleepingOwl\Admin\Facades\FormElement;
use Worthers\SleepingOwlAdminElements\ImageUpload\ImageUpload;
use Worthers\SleepingOwlAdminElements\OptionalDateTime\OptionalDateTime;

class SleepingOwlAdminElementsServiceProvider extends ServiceProvider {
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
		$this->loadViewsFrom(realpath(__DIR__.'/OptionalDateTime/views'), 'worthers.optionalDateTime');
		$this->publishes([
			realpath(__DIR__.'/OptionalDateTime/assets') => public_path('/packages/worthers/sleeping-owl-admin-elements/optional-date-time'),
		], 'public');
		PackageManager::add('sleeping-owl-admin-elements.optional-date-time')
			->js('script', asset('/packages/worthers/sleeping-owl-admin-elements/optional-date-time/script.js'), '', true)
            ->css('style', asset('/packages/worthers/sleeping-owl-admin-elements/optional-date-time/style.css'));
		Meta::loadPackage(['sleeping-owl-admin-elements.optional-date-time']);


        $this->loadViewsFrom(realpath(__DIR__.'/ImageUpload'), 'worthers.imageUpload');
	}

	public function register()
	{
		FormElement::register([
			'optionalDatetime' => OptionalDateTime::class,
            'imageUpload' => ImageUpload::class,
		]);
	}
}
