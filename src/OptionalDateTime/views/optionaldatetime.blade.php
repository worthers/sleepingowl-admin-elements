<div
    class="form-group form-element-date form-element-optionaldatetime {{ $errors->has($name) ? 'has-error' : '' }}"
    data-input-name="{{ $name }}"
>
    <label for="{{ $name }}" class="control-label ">
        {{ $label }}

        @if($required)
            <span class="form-element-required">*</span>
        @endif
    </label>

    <div class="checkbox" style="margin: 0 0 5px 0;">
        <label><input type="checkbox" name="{{ $name }}_active" value="1" {{ $active ? "checked=\"checked\"" : "" }} > Active</label>
    </div>

    <div class="input-date input-group datetime disabled">
        <input
            data-date-format="{{ $pickerFormat }}"
            data-date-pickdate="true"
            data-date-picktime="false"
            data-date-useseconds="{{ $seconds ? 'true' : 'false' }}"
            type="text"
            name="{{ $name }}"
            id="{{ $name }}"
            value="{{ $value }}"
            class="form-control"
            @if($readonly) readonly @endif
        >
        <span class="input-group-prepend input-group-addon">
            <div class="input-group-text">
                <span class="far fa-calendar-alt"></span>
            </div>
        </span>
    </div>

	@include(AdminTemplate::getViewPath('form.element.partials.helptext'))
	@include(AdminTemplate::getViewPath('form.element.partials.errors'))
</div>
