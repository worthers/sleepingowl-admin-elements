
$(function(){
    $('.form-element-optionaldatetime').each(function (index, item) {
        var name = item.getAttribute('data-input-name')
        var checkbox = $('[name="'+name+'_active"]', item);
        var input_group = $('.input-date.input-group.datetime', item);
        var input = $('[name="'+name+'"]',item);

        var updateUi = function(){
            if ( checkbox.is(':checked') ){
                input_group.removeClass('disabled');
                return;
            }
            input_group.addClass('disabled');
        }

        checkbox.change(function(){
            if ( checkbox.is(':checked') ){
                input_group.find('.input-group-addon').click();
            } else {
                input.val('');
            }
            updateUi()
        });

        updateUi();
    })
});
