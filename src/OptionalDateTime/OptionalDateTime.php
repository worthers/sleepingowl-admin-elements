<?php
namespace Worthers\SleepingOwlAdminElements\OptionalDateTime;

use SleepingOwl\Admin\Form\Element\DateTime;
use function view;

/**
 * A sleepingOwl admin element for having an optional datetime field
 *
 * The option datetime field allows the user to activate/deactivate the date field
 *
 * @package Worthers\SleepingOwlAdminElements
 */
class OptionalDateTime extends DateTime
{
	public function toArray()
	{
		return parent::toArray() + [
			'active' => $this->getValueFromModel() ? TRUE : FALSE
		];
	}

	public function render()
	{
		return view(
			'worthers.optionalDateTime::optionaldatetime',
			$this->toArray()
		);
	}
}
